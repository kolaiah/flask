'''
Created on 6 Nov 2016

@author: kimera
'''
from flask import Blueprint, current_app
from database_models import core as c

from mercury.views.api_view import ApiView


def generate_api(app):
    """
    Function that will generate all the views and blueprints
    and corresponding endpoints
    
    The first STEP is go generate the view foe the /api/
    Then for every SQL Alchemy Model, generates the corresponding blueprint
    The name of the blueprint is based on the name table as well as 
    the name of the endpoint. Between the creation of the blueprint 
    and the registration, ApiViews are created
    
    Args:
        :param (object): The Flask Object
    """
    BASE_URI = app.config['API_URL']
    
    list_db_models = [c.Bundle, c.BundleAccount, c.BundleItem, c.Cupon,
                      c.ConfigTokenGeneration, c.CuponToken, c.CustomerAccount,
                      c.Discount, c.HotSpotHub, c.RetailerAccount, c.Token,
                      c.TokenWallet]
    
    for _cls in list_db_models:
        api = ApiView()
         
        # Generates the Blueprints
        bprint = Blueprint(_cls.__tablename__, __name__)
        
        # Regisrer the ApiView
        register_api(api, bprint, _cls)
        
        # Register the blueprints
        app.register_blueprint(bprint, url_prefix = BASE_URI + bprint.name)
        
        
        
def register_api(view, bprint, _cls):
    """
    Used to register the ApiView Objects and add the url rules
    
    Args:
        :param (object): the ApiView object
        :param (object): blueprint to be associated with the view
        :param (object): class corresponding to SQL Alchemy Model
    """
    url_view = view.as_view(bprint.name)

    bprint.add_url_rule('/', 
                     defaults={'table_id': None, 'Model': _cls},
                     view_func=url_view, 
                     methods=['GET', 'POST'])
    
    bprint.add_url_rule('/<table_id>', 
                    defaults={'Model': _cls},
                    view_func=url_view,
                    methods=['GET', 'PUT', 'DELETE'])
    
