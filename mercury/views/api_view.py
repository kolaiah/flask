'''
Created on 6 Nov 2016

@author: kimera
'''
import json
import datetime
from flask import request, current_app
from flask.views import MethodView

from mercury.database import db
from mercury.utils import is_valid_json, return_response_message

class ApiView(MethodView):
    """
    Responsible for every action related with the object views
    """
    
    def __init__(self):
        """
        """
        
    def get(self, table_id, Model):
        """
        Checks if user has the capability to perform the action and then

        Get a record or a list of records
        If there is a table_id provided, then verifies if exists in the DB
        If exists, then returns the object or a list of objects
        Else, returns a response message
        
        Args:
            :param (object): View corresponding of each endpoint
            :param (int): : id to be searched
        
        Returns:
            :return:: single object/list of objects
        """
        _endpoint = str(request.endpoint).split('.')[1]
        try:
            if table_id:
                
                # Gets the record
                model = Model.query.filter_by(id=table_id).first()
                
                if model != None:
                    model_dict = self.model_to_dict(model, Model)
                    
                    # Add to log
                    current_app.logger.info(model_dict)
                    return return_response_message(_endpoint, model_dict, 200)
                
                else:
                    msg = "The ID: " + str(table_id) + ", doesn't exist" +\
                              " in the table " + _endpoint
    
                    current_app.logger.info(msg)
                    return return_response_message(_endpoint, msg, 404)
                    
            else:
                model_list_all = Model.query.all()
                
                if len(model_list_all) != 0:
                    model_list = []
                    
                    for model_result in model_list_all:
                        # Get all in the right Format
                        model_dict = self.model_to_dict(model_result, Model)
                        model_list.append(model_dict)
                    
                    # Add to Logger
                    current_app.logger.info(model_list)
                    return return_response_message(_endpoint, model_list, 200)
                
                else:
                    # The proper status code is 204 but to show a message 
                    # it will be passed 200
                    current_app.logger.error(model_list_all)
                    return return_response_message(_endpoint, model_list_all, 200)
                
        except Exception as e:
            current_app.logger.error(_endpoint + " : " + e.message)
            return return_response_message(_endpoint, e.message, 500)
                    
    
    def post(self, table_id, Model):
        """
        Create a new Model and creates a record
        id is PK and Auto Increment
        
        Args:
            :param (object): View corresponding of each endpoint
        
        Raises:
            :raise Exception: if it can't insert the record into the database
        """
        _endpoint = str(request.endpoint).split('.')[1]
        
        try:
            is_valid_data = is_valid_json(request.data)
            
            if is_valid_data:
                # Transforms in a proper object and not string
                data_dict = json.loads(request.data)
                
                # Inserts into db
                model = Model(**data_dict)
                db.session.add(model)
                db.session.commit()
                
                msg = "Data successfully inserted into " + _endpoint + \
                          ", data: " + json.dumps(data_dict)
                
                # Add to Log
                current_app.logger.info(msg)
                return return_response_message(_endpoint, msg, 200)
                
            else:
                msg = "The data provided is not valid. Table: " + _endpoint\
                          + ", data: " + json.dumps(request.data)
                    
                current_app.logger.error(msg)
                return return_response_message("error", msg, 405)

            
        except Exception as e:
            current_app.logger.error(e.message)
            return return_response_message(_endpoint, e.message, 500)

    
    def put(self, table_id, Model):
        """
        Gets the Model from the id and updates the record

        Args:
            :param (object): View corresponding of each endpoint
            :param (int): : id to be searched and updated

        Raises:
            :raise StaleDataError: A mapped object with version_id_col was 
            refreshed and the version number coming back from the database 
            does not match that of the object itself.
        """
        _endpoint = str(request.endpoint).split('.')[1]
        
        if table_id:
            try:
                is_valid_data = is_valid_json(request.data)
                
                if is_valid_data:
                    
                    # Gets the user to be updated
                    model = Model.query.filter_by(id=table_id).first()
                    
                    if model:
                        # Transforms in a proper object and not string
                        data_dict = json.loads(request.data)
                        
                        # Updates the record
                        Model.query.filter_by(id=table_id).update(data_dict)
                        db.session.commit()
                        
                        msg = "Data successfully updated. " +  "Table: "+ \
                               _endpoint + ", ID: " + str(table_id) +", data: "\
                                + json.dumps(data_dict)
                        
                        current_app.logger.info(msg)
                        return return_response_message(_endpoint, msg, 200)
                    
                    else:
                        msg = "The record doesn't exist. Table" + \
                                  _endpoint + ", ID: "+str(table_id)
                                  
                        current_app.logger.error(msg)
                        return return_response_message(_endpoint, msg , 404)
                
                else:
                    msg = "The data provided is not valid. Data: " +\
                               json.dumps(request.data)
                               
                    current_app.logger.error(msg)
                    return return_response_message(_endpoint, msg, 405)
                    
                
            except Exception as e:
                db.session.rollback()
                current_app.logger.error(e.message)
                return return_response_message(_endpoint, e.message, 500)

        else:
            msg = "You need to provide an ID to update"
            current_app.logger.info(msg)
            return return_response_message(_endpoint, msg, 404)
            
        
    
    def delete(self, table_id, Model):
        """
        Gets the object from the ID provided and remove it
        If there is any error, throws an integrity exception
        
        Args:
            :param (object): View corresponding of each endpoint
            :param (int): : id to be searched and deleted
        
        Raises:
            :raise IntegrityError: The data cannot be removed due relationships 
            with other tables
        """
        _endpoint = str(request.endpoint).split('.')[1]
        
        if table_id:
            
            try:
                # Get the record to delete
                model = Model.query.filter_by(id=table_id).first()
                
                if model:
                    # Removes the model
                    Model.query.filter_by(id=table_id).delete()
                    db.session.commit()
                    
                    msg = "Data successfully deleted." + \
                          "Table" + _endpoint + ", ID: " + str(table_id)
                              
                    current_app.logger.info(msg)
                    return return_response_message(_endpoint, msg, 200)
                
                else:
                    msg = "The record doesn't exist. " + \
                            "Table:" + _endpoint + ", ID: " + str(table_id)
                        
                    current_app.logger.error(msg)
                    return return_response_message("error", msg, 404)
                
            except Exception as e:
                db.session.rollback()
                current_app.logger.error(e.message)
                return return_response_message(_endpoint, e.message, 500)
        
        else:
            msg = "You need to provide an ID to delete"
            current_app.logger.info(msg)
            return return_response_message(_endpoint, msg, 404)
        
    
    def model_to_dict(self, inst, cls):
        """
        Jsonify the sql alchemy query result. Skips attr starting with "_"
        
        Args:
            :param (object): Instance of the class
            :param (object): class to map
        
        Returns:
            :return: dictionary
        """
        convert = { "DATETIME": datetime.datetime.isoformat}
        d = dict()
        for c in cls.__table__.columns:
            if c.name.startswith("_"):
                continue
            v = getattr(inst, c.name)
            current_type = str(c.type)
            if current_type in convert.keys() and v is not None:
                try:
                    d[c.name] = convert[current_type](v)
                except:
                    d[c.name] = "Error:  Failed to covert using ",\
                     unicode(convert[c.type])
            elif v is None:
                d[c.name] = unicode()
            else:
                d[c.name] = v
        return d
        