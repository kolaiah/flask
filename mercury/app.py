'''
Created on 2 Nov 2016

@author: kimera
'''
from flask import Flask, jsonify

from mercury.database import db
from mercury.register_api import generate_api

def create_app(settings_class):
    """
    Mercury - Flask object is created
    
    Args:
        :param settings_class: The object settings for the application
    
    Returns:
        :return: The Flask Object
    """
    
    app = Flask(__name__)
    app.config.from_object(settings_class)
    
    db.init_app(app)
    
    generate_api(app)
    
    from utils import JsonException
    
    # app wide JSON errors
    def handle_json_exception(error):
        if hasattr(error, 'to_dict') and hasattr(error, 'http_status_code'):
            response = jsonify(error.to_dict())
            response.status_code = error.http_status_code
            return response
        elif hasattr(error, 'message'):
            response = jsonify({'msg': error.message})
            return response, 500
        else:
            response = jsonify({'msg': 'Unknown error'})
            return response, 500
    
    app.register_error_handler(JsonException, handle_json_exception)    

    #APP Returns here
    return app

if __name__ == '__main__':
    app = create_app('settings.local_config.Config')
    app.run(debug=app.config['DEBUG'], use_reloader=False, port=5008)