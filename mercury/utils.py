'''
Created on 21 Oct 2016

@author: tiago
'''
import json

from flask import current_app, make_response, jsonify
from __builtin__ import int, str
from random import _inst


class JsonException(Exception):

    def __init__(self, message, http_status_code=400):
        """
        @param:  http_status_code: integer
        @param: : message: str
        """
        Exception.__init__(self)
        self.message = message
        self.http_status_code = http_status_code

        if http_status_code >= 400 and http_status_code != 404:
            current_app.logger.error(message)

    def to_dict(self):
        rv = { 'message' : self.message }
        return rv


class MercuryModel(object):
    """
    Handler to check the parameters in a class model
    """
    def __init__(self, **kwargs):
        """
        """
        cls_ = type(self)
        for k in kwargs:
            if not hasattr(cls_, k):
                raise TypeError("%r is an invalid keyword argument for %s" %
                    (k, cls_.__name__))
            setattr(self, k, kwargs[k])
        
            


def return_response_message(prefix, obj_message, status):
    """
    Handler for JSON responses
    
    :param (str): What is going to be shown where the JSON is generated
    :param (object): Content of the message
    :paran (int): The return status code
    
    Returns:
    :return: The message in JSON Format
    """
    rv = make_response(jsonify({prefix: obj_message}), status)
    rv.mimetype = 'application/json'
    return rv


def is_valid_json(str_data):
    """
    Receives a str and validates if is valid JSON format

    Args:
    :param: (str): Data to be validated if is a valid JSON format or not
    
    :return: boolean if is a valid JSON or not
    """
    try:
        json.loads(str_data)
    except ValueError:
        return False
    return True
