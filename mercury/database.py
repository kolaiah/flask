'''
Created on 5 Nov 2016

@author: kimera
'''
from flask import current_app
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()

def create_database(config_module):

    from mercury.app import create_app
    app = create_app(config_module)
    
    with app.app_context():
        db.create_all()


if __name__ == '__main__':
    # input stdin from the command line
    if raw_input("Create database using local config? y/n ") == 'y':
        print "creating database..."
        
        # import all the models from the database_models
        from mercury.database_models.core import *
        create_database('settings.local_config.Config')
        print "done"
    else:
        msg = "Error whilst creating the database. DB not created!"

        current_app.logger.error(msg)
        print (msg)
