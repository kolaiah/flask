class BaseConfig(object):
    DEBUG = False
    APP_TITLE = "Quartz"
    
    # API Versioning and URI    
    API_PREFIX = ""
    API_VERSION = ""
    API_URL = API_PREFIX + API_VERSION
    
    # SQL ALCHEMY
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = ""
    SQLALCHEMY_DATABASE_URI = "" # really important
    
    # REQUEST urls
    BIOTITE_URL = ""
    GARNET_URL = ""
