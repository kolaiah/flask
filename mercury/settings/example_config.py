from global_config import BaseConfig


class Config(BaseConfig):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:supersecret@localhost/mercury"
    
    
    # API Versioning and URI    
    API_PREFIX = "/mercury/api"
    API_VERSION = "/v1/"
    API_URL = API_PREFIX + API_VERSION
    
