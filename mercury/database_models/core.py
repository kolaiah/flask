'''
Created on 2 Nov 2016

@author: kimera
'''
from sqlalchemy import Column, String, Integer, DateTime, ForeignKey, Numeric,\
                       Boolean
from sqlalchemy.orm import relationship

from mercury.database import db
from mercury.utils import MercuryModel


class RetailerAccount(db.Model, MercuryModel):
    __tablename__ = 'retailers_account'
    id = Column(String(12), primary_key=True)
    email = Column(String(80))
    name = Column(String(45))
    surname = Column(String(45))
    phone_number = Column(String(20))
    mobile_number = Column(String(20))
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<RetailerAccount %r>' % (self.id)


class Bundle(db.Model, MercuryModel):
    __tablename__ = 'bundles'
    id = Column(Integer, primary_key=True)
    total_amount = Column(Numeric(10, 3))
    currency = Column(String(3))
    valid_from = Column(DateTime)
    valid_to = Column(DateTime)
    active = Column(Boolean)
    description = Column(String(60))
    discount_id = Column(Integer, ForeignKey('discounts.id'))

    discount = relationship('Discount', foreign_keys=[discount_id],\
                            backref='bundle')
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<Bundle %r>' % (self.id)
    
    
class BundleAccount(db.Model, MercuryModel):
    __tablename__ = 'bundle_accounts'
    id = Column(String(12), primary_key=True)
    bundle_id = Column(Integer, ForeignKey('bundles.id'))
    retailer_id = Column(String(12), ForeignKey('retailers_account.id'))
    
    bundle = relationship("Bundle", foreign_keys=[bundle_id],\
                          backref='bundle_account')

    retailer = relationship("RetailerAccount", foreign_keys=[retailer_id],\
                            backref='bundle_account')
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<BundleAccount %r>' % (self.id)


class BundleItem(db.Model, MercuryModel):
    __tablename__ = 'bundle_items'
    id = Column(Integer, primary_key=True)
    description = Column(String(30))
    quantity = Column(Integer)
    unitary_price = Column(Numeric(10, 2))
    currency = Column(String(3))
    bundle_id = Column(Integer, ForeignKey('bundles.id'))

    bundle = relationship('Bundle', foreign_keys=[bundle_id],\
                          backref='bundle_item')
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<BundleItem %r>' % (self.id)
    

class Discount(db.Model, MercuryModel):
    __tablename__ = 'discounts'
    id = Column(Integer, primary_key=True)
    email = Column(String(80))
    name = Column(String(45))
    surname = Column(String(45))
    phone_number = Column(String(20))
    mobile_number = Column(String(20))
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<Discount %r>' % (self.id)
    

class Cupon(db.Model, MercuryModel):
    __tablename__ = 'cupons'
    id = Column(Integer, primary_key=True)
    aquired = Column(Boolean)
    validity = Column(DateTime)
    bundle_id = Column(Integer, ForeignKey('bundles.id'))

    bundle = relationship('Bundle', foreign_keys=[bundle_id], backref='cupon')
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<Cupon %r>' % (self.id)
    
    
     
class CustomerAccount(db.Model, MercuryModel):
    __tablename__ = 'customers_accounts'
    id = Column(Integer, primary_key=True)
    email = Column(String(80))
    name = Column(String(45))
    surname = Column(String(45))
    birth_date = Column(DateTime)
    active = Column(Boolean)
    address_1 = Column(String(70))
    address_2 = Column(String(70))
    city = Column(String(45))
    state = Column(String(45))
    country = Column(String(45))
    postal_code = Column(String(15))
    phone_number = Column(String(20))
    mobile_number = Column(String(20))
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<CustomerAccount %r>' % (self.id)


class Token(db.Model, MercuryModel):
    __tablename__ = 'tokens'
    id = Column(String(36), primary_key=True)
    token_type = Column(String(3))
    created_at = Column(DateTime)
    terminated = Column(Boolean)

    hotstop_id = Column(Integer, ForeignKey('hotspot_hubs.id'))

    hotspot = relationship('HotSpotHub', foreign_keys=[hotstop_id],\
                           backref='token')
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<Token %r>' % (self.id)
    

class TokenWallet(db.Model, MercuryModel):
    __tablename__ = 'token_wallets'
    id = Column(Integer, primary_key=True)
    available = Column(Boolean)
    swap_date = Column(DateTime)
    aquired_hub_id = Column(String(45))
    
    token_id = Column(String(36), ForeignKey('tokens.id'))
    cupon_id = Column(Integer, ForeignKey('cupons.id'))
    customer_account_id = Column(Integer, ForeignKey('customers_accounts.id'))
    
    token = relationship('Token', foreign_keys=[token_id],\
                         backref='token_wallet')

    cupon = relationship('Cupon', foreign_keys=[cupon_id],\
                         backref='token_wallet')
    
    customer_account = relationship('CustomerAccount',\
                                    foreign_keys=[customer_account_id],\
                                    backref='token_wallet')
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<TokenWallet %r>' % (self.id)

     
class CuponToken(db.Model, MercuryModel):
    __tablename__ = 'cupon_tokens'
    id = Column(Integer, primary_key=True)
    cupon_id = Column(Integer, ForeignKey('cupons.id'))
    token_id = Column(String(36), ForeignKey('tokens.id'))

    cupon = relationship('Cupon', foreign_keys=[cupon_id],
                         backref='cupon_token')
    
    token = relationship('Token', foreign_keys=[token_id],
                         backref='cupon_token')
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<CuponToken %r>' % (self.id)


class HotSpotHub(db.Model, MercuryModel):
    __tablename__ = 'hotspot_hubs'
    id = Column(Integer, primary_key=True)
    aug_reality = Column(Boolean)
    max_token_day = Column(Boolean)
    longitude = Column(Numeric(18, 12))
    latitude = Column(Numeric(18, 12))
    radius = Column(Numeric(8))
    radius_unit = Column(String(2))
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<HotSpotHub %r>' % (self.id)
                                    
     
class ConfigTokenGeneration(db.Model, MercuryModel):
    __tablename__ = 'config_tokens_generation'
    id = Column(Integer, primary_key=True)
    value_from = Column(Numeric(10, 2))
    value_to = Column(Numeric(10, 2))
    token_required = Column(Integer)
    
    def __init__(self, **kwargs):
        MercuryModel.__init__(self, **kwargs)
    
    def __repr__(self):
        return '<ConfigTokenGeneration %r>' % (self.id)
    
