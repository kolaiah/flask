# Mercury - Restfull Schema #

* SQL file for Mercury project database and ER diagram with relationships between its entities

##  Schema with RESTful Access - CRUD ##

RESTful API and Database Models

## Implementation Notes 

* It's a [Flask](http://flask.pocoo.org/) app with a [Flask-SQLAlchemy](https://pythonhosted.org/Flask-SQLAlchemy/) managed database.
* Please install the requirements needed first from the requirements.txt located in the root of the project.
* The version of python should be 2.7.XXX and not 3.
* To install the dependencies, run the pip command. E.g.: `pip install module`


## Module Installation Note:

* For the module pymysql, you need to change your SQLALCHEMY_DATABASE_URI to `SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://.....'`

## Configuration Settings

Create your own settings file and symbolic link to the manage command.
* If you are running on UNIX/Linux:  

```Shell
[kimera@macbook-pro Mercury]$ cd mercury/settings/
[kimera@macbook-pro settings]$ cp example_config.py local_config.py 
[kimera@macbook-pro settings]$ ln -s local_config_ti.py local_config.py
```

### If you are running on Windows, to set the PYTHONPATH, please follow the steps. This should be run in POWERSHELL

First you get the root location of the project. As an example, the project is located in C:\Users\<user>\Documents\mercury
```Powershell
$env:PYTHONPATH="C:\Users\<user>\Documents\mercury"
```

## Getting Started - Running locally

Create the empty database (permissions are not shown here)-
```SQL
create database mercury;
```

Create an empty database-
```Shell
[kimera@macbook-pro Mercury]$ export PYTHONPATH=`pwd`
[kimera@macbook-pro Mercury]$ cd mercury/
[kimera@macbook-pro mercury]$ python database.py
Create database using local config? y/n y
creating db...
done
```

## File Settings ##

Update this file with your settings

```Python

SQLALCHEMY_DATABASE_URI = "mysql+pymysql://<user>:<password>@<server-address>/mercury"
API_PREFIX = "/mercury/api"  # by default
API_VERSION = "/v1/" #By default we are using v1
```

## Start Application

Then run it
```Shell
[kimera@macbook-pro Mercury]$ export PYTHONPATH=`pwd`
[kimera@macbook-pro Mercury]$ cd mercury/
[kimera@macbook-pro mercury]$ python app.py
```
 * Running on http://127.0.0.1:5008/
